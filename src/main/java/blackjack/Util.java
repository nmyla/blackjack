package blackjack;

/**
 * Place for all Helper functions
 * @author Nagarjuna
 *
 */
public class Util {
	/**
	 * Helper method for styling output to console
	 * @param String str
	 */
	public static void printMsg(String str){
		System.out.println("----------------------------\n"+str+"\n----------------------------");
	}
}
