package blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Entry point to BlackJack Game
 * Responsible to control game flow
 * @author Nagarjuna
 * 
 */
public class BlackJack {
	private Dealer dealer;
	private Shoe shoe;
	private List<Player> players;
	private Scanner sc = new Scanner(System.in);

	/**
	 * Adds a card to the hand
	 * @param Hand hand
	 * @param Card card
	 */
	private void hit(Hand hand, Card card){
		hand.getCards().add(card);
	}
	/**
	 * Sets hand to inactive status
	 * @param Hand hand
	 */

	private void stand(Hand hand){
		hand.setActive(false);
	}

	/**
	 * Doubles the bet amount if player has sufficient balance to double
	 * @param Player player
	 * @param Hand hand
	 * @param Shoe shoe
	 */
	private void doubleDown(Player player,Hand hand,Shoe shoe){
		if(hand.isCanDoubleBet() || hand.getCards().size()==2){
			hand.setCanDoubleBet(true);
			// while adding a new hand to player we deduct initial bet amount. Add it up to check in case if lost player can pay the bet
			if((player.getMoney()+Hand.initialBet)>=(hand.getBet()*2)){
				hand.setBet(hand.getBet()*2);
				hand.getCards().add(shoe.getNextCard());
			}else{
				Util.printMsg("You do not have sufficient balance");
			}
		}else{
			Util.printMsg("You cannot double bet if you haven't done when you have two cards");
		}		
	}

	/**
	 * Allow player to split his hand only if he has sufficient balance and there are only two cards of face value in the hand.
	 * Gives a new card to each hand after splitting
	 * @param Player player
	 * @param Hand hand
	 * @param Shoe shoe
	 */
	private void split(Player player,Hand hand, Shoe shoe){
		if(hand.getCards().size()==2){
			if(player.getMoney()>=Hand.initialBet){
				if(hand.areSameFacevalues()){ //Test for same face value
					Card cardTwo=hand.getCards().get(1);
					hand.addCard(shoe.getNextCard()); // New card to first hand 
					hand.getCards().remove(1); //Remove second card and add it to new hand
					Hand secondHand = new Hand();
					secondHand.addCard(cardTwo);
					secondHand.addCard(shoe.getNextCard()); //New card to second hand
					player.addHand(secondHand); //Add new hand to player
				}else{
					Util.printMsg("Not allowed to split.\n Reason: You should have same face value cards to split");
				}
			}else{
				System.out.println("Not allowed to split.\n Reason: Insufficient balance");
			}
		}else{
			Util.printMsg("You can split hand only when you have two cards");
		}		
	}

	/**
	 * Surrenders the hand and deducts half of bet amount
	 * @param player
	 * @param hand
	 */
	private void surrender(Player player,Hand hand){
		if(hand.getCards().size()==2){
			hand.setActive(false);
			hand.setIssSurrendered(true);
			player.addMoney(Hand.initialBet-hand.getBet()/2); //Deducted total bet amount when new hand is added hence add it back and subtract half of bet amount to player

		}else{
			Util.printMsg("You can surrender only when you have two cards");
		}
	}

	/**
	 * Prompts user for number of players and initializes each player with a hand. Valid number of players are between 1 to 8.
	 * @return List<Player>
	 */
	private List<Player> intializePlayers(){
		int numberofPlayers = 1;
		List<Player> players=new ArrayList<>();
		System.out.println("Please enter number of players: ");	

		do{			
			numberofPlayers =sc.nextInt();
			if(numberofPlayers<1 || numberofPlayers>8){
				System.out.println("Please choose number of players between 1 to 8!!");
			}
		}while(numberofPlayers<1 || numberofPlayers>8);

		for(int i=0;i<numberofPlayers;i++){
			Player player = new Player();
			player.setPlayerIndex(i+1);
			player.addHand();
			players.add(player);
		}
		return  players;

	}

	/**
	 * Prompts user for number of decks to use in the game. Valid values are between 1 to 8
	 * Initializes the Shoe with those many decks and shuffles them
	 * @return Shoe
	 */
	private Shoe intializeShoe(){
		Shoe shoe = new Shoe();
		int numberofDecks =1;
		System.out.println("Please enter number of decks: ");
		do{			
			numberofDecks =sc.nextInt();
			if(numberofDecks<1 || numberofDecks>8){
				System.out.println("Please choose number of decks between 1 to 8!!");
			}
		}while(numberofDecks<1 || numberofDecks>8);

		shoe.setNumberOfDecks(numberofDecks);
		shoe.initializeShoe();
		shoe.shuffle();
		return shoe;
	}

	/**
	 * Initializes the game and responsible for entire game flow
	 */
	private void startBlackJack(){
		dealer = new Dealer();
		shoe = intializeShoe();
		players = intializePlayers();		
		do{
			int choice=0;

			// Give each player and dealer two cards			 
			for(int i=0;i<2;i++){
				for(Player player:players){
					for(Hand hand:player.getHands()){
						hand.getCards().add(shoe.getNextCard());
					}
				}
				dealer.addCard(shoe.getNextCard());
			}

			//Open dealer's 1st card
			System.out.println("Dealer 1st card:");
			dealer.showFirstCard();

			//Check each player's each hand until they stand or surrender or bust or blackjack
			// Allow user to hit or double or split or surrender or stand
			for(int i=0;i<players.size();i++){
				Player player =players.get(i);
				for(int j=0;j<player.getHands().size();j++){
					Hand hand = player.getHands().get(j);
					do{
						//If hand is busted update hand to inactive so we won't ask player any more inputs
						hand.testAndSetBusted();

						//If hand is Blackjack update hand to inactive so we won't ask player any more inputs
						hand.testAndSetBlackJack();

						if(hand.isActive()){								
							System.out.println("Player: "+player.getPlayerIndex()+" Hand: "+hand.getHandIndex());
							hand.showCards();
							System.out.println("Choose your option: \n1.Hit\n2.Stand\n3.Double Down\n4.Split\n5.Surrender\n");
							choice=sc.nextInt();

							switch(choice){
							case 1:
								System.out.println("You chose Hit");
								hit(hand,shoe.getNextCard());
								break;
							case 2:
								System.out.println("You chose Stand");
								stand(hand);								
								break;
							case 3:
								System.out.println("You chose Double Down");
								doubleDown(player,hand,shoe);
								break;
							case 4:
								System.out.println("You chose Split");
								split(player,hand,shoe);								
								break;
							case 5:
								System.out.println("You chose Surrender");
								surrender(player,hand);
								break;
							default:
								System.out.println("Invalid choice try again");
							}							
						}
						if(!hand.isActive()){
							break;
						}
					}while(hand.isActive() || choice<1 || choice >5);
				}
			}

			// Give cards to dealer until his count is less than 17
			while(dealer.getHand().getTotal()<17){
				dealer.addCard(shoe.getNextCard());
			}	

			displayGameResults();
			replayPromptPlayerFilter();			
			dealer.resetHand();
			
		}while(players.size()>0);
		Util.printMsg("No more players left!! Restart game to play again");
		sc.close();
	}

	/**
	 * Displays game results
	 */
	private void displayGameResults(){
		System.out.println("Dealer:");
		dealer.getHand().showCards();
		for(Player player:players){
			for(Hand hand:player.getHands()){
				Hand.Result result= hand.getResult(dealer.getHand());
				StringBuilder sb = new StringBuilder();
				sb.append("Player: "+ player.getPlayerIndex() +" Hand: "+hand.getHandIndex());
				if(!hand.isSurrendered()){
					if(result==Hand.Result.LOST){						
						if(hand.getBet()>Hand.initialBet){ 
							// Player might have doubled the bet amount hence calculate it properly.
							player.addMoney(Hand.initialBet - hand.getBet());
						}
						sb.append(" Lost");
					}else if(result==Hand.Result.WON){ 
						// Player won so pay back initial bet and actual bet value
						player.addMoney(Hand.initialBet + hand.getBet());
						sb.append(" Won");
					}else if(result==Hand.Result.BLACKJACK){ 
						// Player got blackjack pay back initial bet and 1.5 times bet value
						player.addMoney(Hand.initialBet + 1.5*hand.getBet());
						sb.append(" BlackJack");
					}else if(result==Hand.Result.PUSH){	
						// Player got a push pay back initial bet 
						player.addMoney(Hand.initialBet);
						sb.append(" PUSH");
					}
				}else{
					sb.append(" Surrendered");
				}

				sb.append("\nYour balance is: "+player.getMoney());
				Util.printMsg(sb.toString());
				hand.showCards();
			}
		}
	}

	/**
	 * Checks if each player has balance to replay
	 * Filters out players who do no have sufficient balance
	 * Filters out players who do not want to play based on player's input
	 */
	private void replayPromptPlayerFilter(){
		for(int i=0;i<players.size();){
			Player player = players.get(i);
			System.out.println("Player: "+player.getPlayerIndex());			
			System.out.println("Do you want to play again(y/n)?");
			String playOrNot = sc.next();

			if(!"y".equals(playOrNot)){
				players.remove(i);
			}else{
				player.resetHand();
				if(!player.addHand()){
					Util.printMsg("You have low balance. You are out of game");
					players.remove(i);
					continue;
				}else{
					i++;
				}
			}			
		}	
	}

	/**
	 * Main entry point to the start the game
	 * @param args
	 */
	public static void main(String[] args) {
		BlackJack blackJack = new BlackJack();
		blackJack.startBlackJack();
	}
}
