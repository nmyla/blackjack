package blackjack;

import java.util.ArrayList;
import java.util.List;

import blackjack.Card.Type;

/**
 * Hold cards
 * Maintains hand status, bet
 * Analyzes results 
 * @author Nagarjuna
 *
 */
public class Hand{
	// Default bet given as 10
	public final static double initialBet=10;
	
	// Holds current bet value
	private double bet=initialBet;
	
	/*Stores whether current Hand is active state which mean either playing hit/double/split
	 Once after player chooses to stand/surrender or got bust/blackjack player is inactive which mean we don't need to server cards to player any more*/
	private boolean isActive=true;
	
	// Stores list of cards in this hand
	private List<Card> cards=new ArrayList<>();
	
	// Used to track if hand has been surrendered
	private boolean isSurrendered = false;
	
	// Used to track if user can double bet. Player looses chance to double bet if player hadn't chosen while he has 2 cards
	private boolean canDoubleBet = false;
	
	// Used to track player hands
	private int handIndex=1;
	
	/**
	 * Returns Hand index
	 * @return int
	 */
	public int getHandIndex() {
		return handIndex;
	}
	
	/**
	 * Set Hand index
	 * @param int handIndex
	 */

	public void setHandIndex(int handIndex) {
		this.handIndex = handIndex;
	}
	
	/**
	 * Enum to identify result
	 * @author Nagarjuna
	 *
	 */
	public enum Result{
		NONE(0),LOST(1),WON(2),BLACKJACK(3),PUSH(4);
		private int value;
		private Result(int value){
			this.value=value;
		}
	}

	/**
	 * Returns the list of cards in hand
	 * @return List<Card>
	 */
	public List<Card> getCards() {
		return cards;
	}

	/**
	 * Adds card to current hand
	 * @param Card card
	 */
	public void addCard(Card card){
		cards.add(card);
	}
	
	/**
	 * Return current bet amount
	 * @return double
	 */
	public double getBet() {
		return bet;
	}

	/**
	 * Sets current bet amount
	 * @param double bet
	 */
	public void setBet(double bet) {
		this.bet = bet;
	}

	/**
	 * Returns if hand had been surrendered or not
	 * @return boolean
	 */
	public boolean isSurrendered() {
		return isSurrendered;
	}
	
	/**
	 * Set players surrendered or not
	 * @param boolean isSurrendered
	 */
	public void setIssSurrendered(boolean isSurrendered) {
		this.isSurrendered = isSurrendered;
	}

	/**
	 * Returns if hand can double bet amount or not
	 * @return boolean
	 */
	public boolean isCanDoubleBet() {
		return canDoubleBet;
	}
	
	/**
	 *  Set can double bet amount or not
	 * @param boolean canDoubleBet
	 */
	public void setCanDoubleBet(boolean canDoubleBet) {
		this.canDoubleBet = canDoubleBet;
	}
	
	/**
	 * Returns if hand is active or not
	 * @return boolean
	 */
	public boolean isActive() {
		return isActive;
	}
	
	/**
	 * Set hand active status
	 * @param boolean isActive
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	/**
	 * Checks if hand is having a two cards with same face value or not
	 * @return boolean
	 */
	public boolean areSameFacevalues(){
		if(cards.size()==2){
			if(cards.get(0).getFaceValue()==cards.get(1).getFaceValue()){
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if hand is Blackjack or not
	 * @return boolean
	 */
	public boolean isBlackJack(){
		if(getTotal()==21 ){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if hand got busted or not
	 * @return boolean
	 */
	public boolean isBust(){
		if(getTotal()>21 ){
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if hand got busted or not and updates hand active status, displays cards and relavent message to player
	 */
	public void testAndSetBusted(){
		if(isBust()){
			setActive(false);
			showCards();	
			Util.printMsg("You are BUSTED");
		}
	}
	/**
	 * Checks if hand got busted or not and updates hand active status, displays cards and relavent message to player
	 */
	public void testAndSetBlackJack(){
		if(isBlackJack()){
			showCards();
			setActive(false);
			Util.printMsg("You got BLACKJACK !!!");
		}
	}
	
	/**
	 * Calculates total face value of cards in hand
	 * @return int
	 */
	public int getTotal(){
		int total=0;
		for(Card card:cards){
			if(card.getType()==Type.ACE){
				total +=11;
			}else{
				total+=card.getFaceValue();
			}
		}
		return total;
	}

	/**
	 * Calculates the player's hand result by comparing with dealer's hand
	 * If player's hand total sum of cards face value greater than 21 -> Lost
	 * If dealer's hand and player's hand both have BlackJack -> Push
	 * If player's hand has BlackJack and dealer hand doesn't have -> BlackJack
	 * If dealer's hand in total greater than 21 or player hand total is greater than dealer's hand total -> Won
	 * If player's hand in total lesser than dealer's hand total -> Lost
	 * If player's hand in total equal to dealer's hand total -> Push
	 * By default return's none
	 * @param Hand dealerHand
	 * @return Result
	 */
	public Result getResult(Hand dealerHand){
		int handTotal = getTotal();
		if(getTotal()>21){
			return Result.LOST;
		}else if(dealerHand.isBlackJack()&&isBlackJack()){
			return Result.PUSH;
		}else if(isBlackJack() && !dealerHand.isBlackJack()){
			return Result.BLACKJACK;
		}else if(dealerHand.getTotal()>21 || getTotal()>dealerHand.getTotal()){
			return Result.WON;
		}else if(getTotal()<dealerHand.getTotal()){
			return Result.LOST;
		}else if(getTotal()==dealerHand.getTotal()){
			return Result.PUSH;
		}
		return Result.NONE;
	}
	
	/**
	 * Displays card's in hand and total
	 */
	public void showCards(){
		StringBuilder sb = new StringBuilder();
		sb.append("Your cards:\n************************\n");
		for(Card card:getCards()){
			sb.append(card.getFaceValue()+" ");
		}
		sb.append("\nTotal: "+getTotal());
		sb.append("\n************************");
		System.out.println(sb.toString());
	}

	@Override
	public String toString() {
		return "Hand [bet=" + bet + ", isActive=" + isActive + ", cards=" + cards + ", isSurrendered=" + isSurrendered
				+ ", handIndex=" + handIndex + "]";
	}
}
