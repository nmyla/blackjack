package blackjack;

import java.util.ArrayList;
import java.util.List;

/**
 * Player holds multiple hands
 * Tracks balance
 * We do not need to have player and dealer being inherited from common parent as there are no similar functionality or characteristics
 * @author Nagarjuna
 *
 */
public class Player{
	// List to store multiple hands
	private List<Hand> hands=new ArrayList<>();

	// Initial money player is allocated
	public static final double initialMoney=100;

	// To track the players playing
	private int playerIndex=1;

	// Current balance player has
	private double money=initialMoney;

	/**
	 * Returns player index
	 * @return int
	 */
	public int getPlayerIndex() {
		return playerIndex;
	}

	/**
	 * Set player index
	 * @param int playerIndex
	 */
	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	/**
	 * Returns current balance
	 * @return double
	 */
	public double getMoney() {
		return money;
	}

	/**
	 * Set current balance
	 * @param double money
	 */
	public void setMoney(double money) {
		this.money = money;
	}

	/**
	 * Returns hands as list
	 * @return List<Hand>
	 */
	public List<Hand> getHands() {
		return hands;
	}

	/**
	 * Creates a new hand and adds to current hand list if player has sufficient balance
	 * Deducts initial bet amount on successful adding hand
	 * Returns true or success else false
	 * @return boolean
	 */
	public boolean addHand(){
		return addHand(new Hand());
	}

	/**
	 * Adds hand input to current hand list if player has sufficient balance
	 * Deducts initial bet amount on successful adding hand
	 * Returns true or success else false
	 * @param Hand hand
	 * @return boolean
	 */
	public boolean addHand(Hand hand){
		hand.setHandIndex(hands.size()+1);
		if(money<Hand.initialBet){
			System.out.println("You do not have sufficient balance to add new hand");
			return false;
		}
		// Adding a new hand mean you have to bet on it and hence deduct the bet amount
		money -=Hand.initialBet;
		hands.add(hand);
		return true;
	}

	/**
	 * Clears current hand list
	 */
	public void resetHand(){
		hands.clear();
	}

	/**
	 * Adds the input amount to current balance
	 * To subtract pass negative amount
	 * @param double amount
	 */
	public void addMoney(double amount){
		money += amount;
	}

	@Override
	public String toString() {
		return "Player [hands=" + hands + ", playerIndex=" + playerIndex + ", money=" + money + "]";
	}
}
