package blackjack;

/**
 * Dealer to host and run game
 * We do not need to have player and dealer being inherited from common parent as there are no similar functionality or characteristics
 * @author Nagarjuna
 *
 */
public class Dealer {
	// Holds dealer cards
	private Hand hand=new Hand();
	
	/**
	 * Adds  a card to the hand
	 * @param Card card
	 */
	public void addCard(Card card){
		hand.getCards().add(card);
	}
	
	/**
	 * Returns dealer hand
	 * @return Hand
	 */
	public Hand getHand() {
		return hand;
	}
	
	/**
	 * Displays dealer's first card
	 */
	public void showFirstCard(){
		if(hand.getCards().size()>1){
			System.out.println(hand.getCards().get(0).getFaceValue()+"\n");
		}else{
			System.out.println("Dealer doesn't have any cards\n");
		}
	}
	
	/**
	 * Resets dealer hand to initial state
	 */
	public void resetHand(){
		hand=new Hand();
	}

	@Override
	public String toString() {
		return "Dealer [hand=" + hand + "]";
	}
}
