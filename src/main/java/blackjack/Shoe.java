package blackjack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Holds cards
 * Initializes cards
 * Shuffles cards
 * Returns next card
 * @author Nagarjuna
 *
 */
public class Shoe {
	// Store all cards in list
	private List<Card> cards = new ArrayList<>();
	
	// Number of decks to use
	private int numberOfDecks=1;
	
	//Track current card index to serve
	private int currentCardIndex=0;
	
	/**
	 *  Initializes deck and shuffles the deck
	 */
	public Shoe(){
		initializeShoe();
		shuffle();
	}
	
	/**
	 * 
	 * @param int numberOfDecks
	 */
	public void setNumberOfDecks(int numberOfDecks) {
		this.numberOfDecks = numberOfDecks;
	}
	
	/**
	 * Initializes shoe with total cards of 52 times number of decks
	 */
	public void initializeShoe(){
		for(int i=0;i<numberOfDecks;i++){
			for(int j=0;j<4;j++){
				for(int k=0;k<13;k++){
					cards.add(new Card( Card.Type.values()[k]));
				}
			}
		}
	}
	
	/**
	 * Shuffles the deck and resets currentCardIndex to start
	 */
	public void shuffle(){
		Collections.shuffle(cards, new Random(System.nanoTime()));
		currentCardIndex=0;
	}
	/**
	 * Returns the next card from the shoe
	 * If currentCardIndex exceeds list size will shuffles and returns cards from starting index
	 * @return Card
	 */
	public Card getNextCard(){
		if(currentCardIndex >= cards.size()){
			shuffle();
		}
		return cards.get(currentCardIndex++);
	}

	@Override
	public String toString() {
		return "Shoe [cards=" + cards + ", numberOfDecks=" + numberOfDecks + ", currentCardIndex=" + currentCardIndex
				+ "]";
	}
}
