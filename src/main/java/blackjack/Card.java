package blackjack;

/**
 * Represent a Card
 * In BlackJack we need only card value hence no need to maintain suits
 * @author Nagarjuna
 *
 */
public class Card{
	private Type type;
	// Stores apparent card value and we are interested in this value only
	private int faceValue;

	/**
	 * Enum to store different card types
	 * @author Nagarjuna
	 *
	 */
	public enum Type{
		ACE(1),TWO(2),THREE(3),FOUR(4),FIVE(5),SIX(6),SEVEN(7),EIGHT(8),NINE(9),TEN(10),JACK(11),QUEEN(12),KING(13);
		private int value;
		private Type(int value){
			this.value=value;
		}
	}

	/**
	 * Initialize card and set face value based on card type
	 * @param type
	 */
	public Card( Type type){
		this.type=type;
		faceValue = type.value;
		if(faceValue==1){
			faceValue=11;
		}else if(faceValue > 10){
			faceValue=10;
		}
	}

	/**
	 * Returns card type
	 * @return
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Returns card face value
	 * @return
	 */
	public int getFaceValue() {
		return faceValue;
	}

	@Override
	public String toString() {
		return "Card [type=" + type + ", faceValue=" + faceValue + "]";
	}

}
