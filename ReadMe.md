# BlackJack
BlackJack Game

## Play Instructions
* Make sure you have JRE version 1.5 or higher version installed
* Execute `blackjack-0.1.jar` located in target folder using command `java -jar blackjack-0.1.jar`
* Answer input prompts to start the game
* Each player at the start of game will be given $100 balance
* Each hand he adds will be $10 as bet
* At each stage you can choose Hit or Stand or Double Down or Split or Surrender based on balance and game rules
* Hit can be chosen if player's current hand total is less than 21
* Stand can be chosen at any point of time while score is less than 21.
* Double down can be chosen only if player's hand had chosen to go for double when he had two cards. If he had already chosen double down once he can go for it any number of times until his hand is in active. 
* Split can be chosen only if hand has two cards and are of same value.
* Surrender can be chosen only if having two cards not any later.
* For more game rule refer to `http://www.pagat.com/banking/blackjack.html`
```
Each hand will result in one of the following events for the player:

 * Lose - the player's bet is taken by the dealer.
 * Win - the player wins as much as he bet. If you bet $10, you win $10 from the dealer (plus you keep your original bet, of course.)
 * Blackjack (natural) - the player wins 1.5 times the bet. With a bet of $10, you keep your $10 and win a further $15 from the dealer.
 * Push - the hand is a draw. The player keeps his bet, neither winning nor losing money.
```

## Developer instructions
* Install JDK 1.5 or higher version if not done
* Import as Maven project into eclipse or any other IDE
* Run as application